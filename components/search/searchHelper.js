import {findPostByText} from "@/utils/prismic";

export const getStoriesList = async (query) => {
	const results = await findPostByText("document.type", "post", "document", query);
	if (results.length) {
		return results.reduce((acc,cur) => {
			return [...acc, {uid: cur.uid, ...cur.data}, { divider: true, inset: false }]
		},[{ header: "Найдено:", found: true }])
	} else return [{ header: "По вашему запросу ничего не найдено", found: false }];
}
