import Vue from "vue"

Vue.mixin({
	computed: {
		$customTheme() {
			return this.$vuetify.theme.themes[this.$vuetify.theme.dark ? "dark" : "light"]
		},
	},
	methods: {
		$meta_head(payload) {
		  const {pageType, title, uid, description, keywords, imageUrl } = payload
			return {
				title,
				link: [
					{
						rel: "canonical",
						href: `https://screep.ru/news/${uid}`
					}
				],
				meta: [
					{
						hid: "description",
						name: "description",
						content: description
					},
					{
						hid: "keywords",
						name: "keywords",
						keywords: keywords
					},
					// Open Graph
					{ hid: "og:title", name: "og:title", content: title },
					{ hid: "og:description", name: "og:description", content: description },
					{ hid: "og:type", name: "og:type", content: "website" },
					{ hid: "og:url", name: "og:url", content: `https://screep.ru/${pageType}/${uid}`},
					{ hid: "og:image", name: "og:image", content: `${imageUrl}`}
				],
			}
		},
	}
});
