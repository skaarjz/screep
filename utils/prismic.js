import Prismic from "prismic-javascript";
import PrismicDom from "prismic-dom"
import config from "../config/prismic.config";
import axios from "axios"

const _initApi = async() => await Prismic.getApi(config.apiEndpoint, {accessToken: config.accessToken});


export const getAllPosts = async (documentType, value, pageSize = 0, page = 1) => {
  	const api = await _initApi();
  	return await api.query(
  		Prismic.Predicates.at(documentType, value),
  		{ orderings : "[document.first_publication_date desc]", pageSize, page }
  	);
};

export const getTopPosts = async(documentType, value, pageSize = 0, page = 1 ) => {
  	const getTopPostsUIDs = await axios.get("/blog/posts/top", {
  		baseURL: process.env.API_URL
  	});
  	const topUIDs = getTopPostsUIDs.data.map(item => item.pid);
  	const api = await _initApi();
  	return await api.query(
  		Prismic.Predicates.in("document.id", topUIDs), { pageSize, page }
	);
};

export const getPostByUid = async(type, uid) => {
  	const api = await _initApi();
  	return await api.getByUID(type, uid)
};

export const findPostByText = async(documentType, value, type, text) => {
  	const api = await _initApi();
  	const {results} = await api.query([
  		Prismic.Predicates.at(documentType, value),
  		Prismic.Predicates.fulltext(type, text),
  	], { pageSize : 10,   orderings : "[document.last_publication_date desc]" });
  	return results
};

export const getPostsByTag = async(documentType, tag, pageSize = 0, page = 1) => {
  	const api = await _initApi();
  	return api.query([
  		Prismic.Predicates.at(documentType, [tag])
  	], {  orderings : "[document.last_publication_date desc]", pageSize, page })
};

export const getPostsByField = async(field) => {
  	const api = await _initApi();
  	return api.query([
  		Prismic.Predicates.at("document.type", "post"),
  		Prismic.Predicates.at("my.post.results.data.category.name", "Разное")
  	]);
};

export const getDataForPage = async (documentType, postUid) => {
  	try {
  		const {
  			data: {
  				post_title,
  				post_body,
  				post_image,
  				meta_keywords,
  				meta_description,
  				meta_title
  			},
  			id,
  			first_publication_date,
  			tags
  		} = await getPostByUid(documentType, postUid);
  		return {
  			header: PrismicDom.RichText.asText(post_title),
  			content: PrismicDom.RichText.asHtml(post_body),
  			date: first_publication_date,
  			logo: post_image,
  			tags,
  			id,
  			meta_keywords,
  			meta_title: PrismicDom.RichText.asText(meta_title),
  			meta_description: PrismicDom.RichText.asText(meta_description)
  		}
  	} catch(e) {
  		error({ statusCode: 404 })
  	}
}




