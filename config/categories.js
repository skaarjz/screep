export const categories = {
	other: "Прочее",
	ghosts: "Призраки",
	monsters: "Монстры",
	demons: "Демоны",
	vampires: "Вампиры"
};
