import colors from "vuetify/lib/util/colors"
import {getAllPosts} from "./utils/prismic";
require("dotenv").config();

export default {
	generate: {
		async routes () {
			const {results} = await getAllPosts("document.type", "post")
			return await Promise.all(results.map((item) => {
				return {
					route: `/story/${item.uid}`
				}
			}))
		}
	},
	target: "static",
	/*
  ** Headers of the page
  */
	head: {
		htmlAttrs: {
			lang: "ru"
		},
		title: "Страшные истории - блог для любителей мистики и жути",
		meta: [
			{ charset: "utf-8" },
			{ name: "telegram:channel", content: "@screepru" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" },
			{ hid: "description", name: "description", content: "Страшные, жуткие, таинственные и мистические истории. Реальные и не очень..." }
		],
		link: [
			{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }
		],
	},
	env: {
		baseUrl: process.env.API_URL || "http://localhost:4000"
	},
	plugins: [
		{ src: "~/plugins/clipboard" },
		{ src: "~/plugins/mixins" },
		{ src: "~/plugins/disqus" }
	],
	buildModules: ["@nuxtjs/vuetify", "@nuxtjs/dotenv", "@nuxtjs/axios", ["@nuxtjs/date-fns", { locale: "ru" }]],
	modules: ["@nuxtjs/sitemap", "@nuxtjs/auth", "@nuxtjs/recaptcha"],
	axios: {
		baseURL:  process.env.API_URL
	},
	auth: {
		strategies: {
			local: {
				endpoints: {
					login: { url: `${process.env.API_URL}/auth/signIn`, method: "post", propertyName: "accessToken" },
					logout: false,
					user: { url: `${process.env.API_URL}/auth/me`, method: "get", propertyName: false}
				},
				tokenType: "bearer",
				cookie: false,
				rewriteRedirects: true
			}
		},
		redirect: {
			home: false
		},
	},
	sitemap: {
		hostname: "https://screep.ru",
		gzip: true
	},
	vuetify: {
		/* module options */
		customVariables: ["~/assets/variables.scss"],
		theme: {
			dark: true,
			themes: {
				light: {
					background: colors.grey.lighten3,
					menu: colors.grey.lighten4,
					card: colors.grey.lighten4,
					color: colors.grey.darken4,
					storyFooter: colors.grey.lighten4,
					spinner: colors.grey.darken2,
					cardHover: colors.grey.lighten4,
					cardHoverColor: "#121212",
					menuList: "#F4A460"
				},
				dark: {
					spinner: colors.grey.lighten4,
					cardHover: "#121212",
					cardHoverColor: "#fff",
					menuList: "#A52A2A"
				}
			}
		},
		defaultAssets: false,
		treeShake: true,
	},
	recaptcha: {
		hideBadge: true, // Hide badge element (v3 & v2 via size=invisible)
		language: "ru",   // Recaptcha language (v2)
		siteKey: "6Lc2c-sUAAAAAAkbC5sGVzMEykb_ch0Dp2AD2s_E",    // Site key for requests
		version: 2,     // Version
		size: "invisible"        // Size: 'compact', 'normal', 'invisible' (v2)
	},
	/*
  ** Customize the progress bar color
  */
	loading: {
		color: "#ff0000"
	},
	/*
  ** Build configuration
  */
	build: {
		/*
    ** Run ESLint on save
    */
		html: { minify: { collapseWhitespace: true }},
		extend (config, { isDev, isClient }) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: "pre",
					test: /\.(js|vue)$/,
					loader: "eslint-loader",
					exclude: /(node_modules)/
				})
			}
		}
	}
}
